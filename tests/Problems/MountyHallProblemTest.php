<?php

namespace App\Tests\Problems;

use App\Problems\MountyHall;
use PHPUnit\Framework\TestCase;

class MountyHallProblemTest extends TestCase
{
    public function testItShouldHaveTwoThirdsProbabilityIfYouSwitchDoors()
    {
        $problem = new MountyHall();
        $this->assertGreaterThanOrEqual(2/3, $problem->getProbability(1000));
    }

    public function testFoo()
    {
        $problem = new MountyHall();
        $problem->getOptimalIterationCountForTwoThirdsProbability(10);
    }
}
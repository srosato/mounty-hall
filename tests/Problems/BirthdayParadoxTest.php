<?php

namespace App\Tests\Problems;

use App\Problems\BirthdayParadox;
use PHPUnit\Framework\TestCase;

class BirthdayParadoxTest extends TestCase
{
    public function testItShouldHaveFiftyPercentProbabilityOn23People()
    {
        $paradox = new BirthdayParadox();
        $probability = $paradox->getProbabilityForSameAnniversary(23, 10000);

        $this->assertGreaterThan(0.5, $probability);
        $this->assertLessThan(0.51, $probability);
    }

    public function testItShouldHaveNearlyHundredPercentProbabilityOn70People()
    {
        $paradox = new BirthdayParadox();
        $probability = $paradox->getProbabilityForSameAnniversary(70, 10000);

        $this->assertGreaterThanOrEqual(0.99, $probability);
    }
}
<?php

namespace App\Command;

use App\Problems\MountyHall;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MountyHallProblem extends Command
{
    protected function configure()
    {
        $this->setName('app:mounty-hall');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $problem = new MountyHall();
//        $this->probability($output, $problem);
        $this->optimalIterationCount($output, $problem);
    }

    protected function optimalIterationCount(OutputInterface $output, MountyHall $problem): void
    {
        $result = $problem->getOptimalIterationCountForTwoThirdsProbability(100000);
        $output->writeln("Min: ${result['min']}, Max: ${result['max']}");

        foreach ($result['distribution_probability'] as $numberOfTries => $distributionProbability) {
            $output->writeln("$numberOfTries: $distributionProbability ({$result['distribution'][$numberOfTries]})");
        }
    }

    protected function probability(OutputInterface $output, MountyHall $problem): void
    {
        $probability = $problem->getProbability(50);
        $output->writeln("Win ratio: ${probability}");
    }

}
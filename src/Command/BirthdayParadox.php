<?php

namespace App\Command;

use App\Problems\BirthdayParadox as BirthdayParadoxProblem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BirthdayParadox extends Command
{
    protected function configure()
    {
        $this->setName('app:birthday-paradox');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $problem = new BirthdayParadoxProblem();
        $output->writeln($problem->getProbabilityForSameAnniversary(70));
    }
}
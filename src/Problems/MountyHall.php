<?php

namespace App\Problems;

class MountyHall
{
    const NUMBER_OF_DOORS = 3;

    public function getProbability($numberOfIterations = 1000)
    {
        $wins = 0;

        for ($i = 0; $i < $numberOfIterations; $i++) {
            $doorWithCar = rand(1, self::NUMBER_OF_DOORS);
            $doorChosen = rand(1, self::NUMBER_OF_DOORS);

            $doorToShow = $this->findDoorToShow($doorChosen, $doorWithCar);
            $doorThatWeCanSwitchTo = $this->findDoorThatWeCanSwitchTo($doorChosen, $doorToShow);

            if ($doorThatWeCanSwitchTo === $doorWithCar) {
                $wins++;
            }
        }

        $winRatio = $wins / $numberOfIterations;
        return $winRatio;
    }

    public function getOptimalIterationCountForTwoThirdsProbability($numberOfIterations = 1000)
    {
        $numberOfIterationsThatItDid = [];
        for ($i = 0; $i < $numberOfIterations; $i++) {
            $numberOfTries = 3;
            do {
                $probability = $this->getProbability($numberOfTries++);
            } while ($probability < 2 / 3);

            $numberOfIterationsThatItDid[] = $numberOfTries;
        }

        $distribution = array_count_values($numberOfIterationsThatItDid);
        ksort($distribution);

        $distributionProbability = [];
        foreach ($distribution as $numberOfTries => $count) {
            $distributionProbability[$numberOfTries] = $count / $numberOfIterations;
        }

        return [
            'min' => min($numberOfIterationsThatItDid),
            'max' => max($numberOfIterationsThatItDid),
            'distribution' => $distribution,
            'distribution_probability' => $distributionProbability,
        ];
    }

    private function findDoorThatWeCanSwitchTo($doorChosen, $doorToShow): int
    {
        $doors = range(1, self::NUMBER_OF_DOORS);
        unset($doors[array_search($doorChosen, $doors)], $doors[array_search($doorToShow, $doors)]);

        return array_shift($doors);
    }

    protected function findDoorToShow($doorChosen, $doorWithCar): int
    {
        do {
            $doorToShow = rand(1, self::NUMBER_OF_DOORS);
        } while ($doorToShow === $doorChosen || $doorToShow === $doorWithCar);
        return $doorToShow;
    }
}
<?php

namespace App\Problems;

class BirthdayParadox
{
    public function getProbabilityForSameAnniversary(int $numberOfPeople, int $numberOfIterations = 1000)
    {
        $sameBirthdays = 0;

        for ($i = 0; $i < $numberOfIterations; $i++) {
            $birthdays = [];
            for ($j = 0; $j < $numberOfPeople; $j++) {
                $birthdays[] = rand(1, 365);
            }

            if (count($birthdays) !== count(array_unique($birthdays))) {
                $sameBirthdays++;
            }
        }

        return $sameBirthdays / $numberOfIterations;
    }
}